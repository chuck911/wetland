<?php

class WebApp {
	protected $view;
	protected $routes = array();
	public $baseDir = './';
	public $defaultTemplate = 'test2.html';
	public $prefix = '/wetland';

	public function __construct() {
		$this->view = new View();
		$this->recordSpider();
	}

	public function run() {
		// $URI = ltrim($_SERVER['REQUEST_URI'],'/');
		$URI = $_SERVER['REQUEST_URI'];
		$URI = strtok($URI, '?');
		if ($URI==$this->prefix) $URI.='/';
		foreach($this->routes as $route) {
			$route['pattern'] = preg_replace('/{(\w+)}/', '(\w+)', $route['pattern']);
			$route['pattern'] = $this->prefix.$route['pattern'];
			// var_dump('#^'.$route['pattern'].'$#');
			if(preg_match('#^'.$route['pattern'].'$#', $URI, $matches)) {
				
				array_shift($matches);
				call_user_func_array($route['func'],$matches);
				exit();
			}
		}
		// $this->autoGenerate();
		$this->notFound();
	}

	public function notFound() {
		header("HTTP/1.0 404 Not Found");
		require('../templates/404.html');
		exit();
	}

	public function route($pattern, $func, $method='GET') {
		array_push($this->routes, compact('pattern', 'func'));
	}

	protected function recordSpider() {
		$ua = $_SERVER['HTTP_USER_AGENT'];
		$spiders = ['Baiduspider','Sogou','360Spider'];
		foreach($spiders as $spider) {
			if (strpos($ua,$spider)!==FALSE) {
				$fp = fopen('../logs/spider.csv', 'a');
				fputcsv($fp, [date('Y-m-d H:i:s'), $ua, $_SERVER['REQUEST_URI']]);
				fclose($fp);
				return;
			}
		}
	}

	protected function autoGenerate() {
		$URI = ltrim($_SERVER['REQUEST_URI'],'/');
		$URI = strtok($URI, '?');
		
		$pathInfo = pathinfo($URI);
		if (!isset($pathInfo['extension'])) {
			$URI.='/index.html';
			$pathInfo = pathinfo($URI);
		} else {
			if (!in_array($pathInfo['extension'],array('html','xml'))) {
				header("HTTP/1.0 404 Not Found");
				exit();
			}
		}
		$filePath = $this->baseDir.$URI;
		$dirPath = $this->baseDir.$pathInfo['dirname'];
		//FORCE_REGENERATE ?
		if (!file_exists($filePath)) {
			if (!is_dir($dirPath)) {
				mkdir($dirPath,0755,true);
			}
			$content = $this->view->render($this->defaultTemplate);
			file_put_contents($filePath, $content);
			echo $content;
		} else {
			echo file_get_contents($filePath);
		}
	}
}