<?php

class Controller {
	protected $view;

	public function __construct() {
		$this->view = new View();
	}

	public function post($tag, $id) {
		// $files = glob('../posts/'.$tag.'/*.txt');
		// $file = $files[rand(0,count(files)-1)];
		$file = "../posts/{$tag}/{$id}.json";
		if (!file_exists($file)) {
			$this->notFound();
		}
        // $data = json_decode(file_get_contents($file),true);
        $data['post'] = new Post($file);
		$data['relatedPosts'] = $this->randomPosts(8);
        $data['next'] = $this->randomPosts(1);
		// var_dump($data['relatedPosts']);
		echo $this->view->render('post.html', $data);
	}

	public function index() {
        $data = array();
        $data['tags'] = Tag::all();
		$data['newPosts'] = $this->randomPosts(20);
		$data['hotPosts'] = $this->randomPosts(10);
		echo $this->view->render('index.html', $data);
    }
    
    public function tag($tag) {
        $tags = require('../dbs/tags.php');
        if (!isset($tags[$tag])) {
            $this->notFound();
        }
        $data = array();
        $data['tagName'] = $tags[$tag];
        $files = glob('../posts/'.$tag.'/*.json');
		$data['newPosts'] = $this->randomPosts(20, $files);
		$data['hotPosts'] = $this->randomPosts(10);
		echo $this->view->render('tag.html', $data);
	}

	protected function randomPosts($num = 1, $files=null) {
		if (!$files) $files = glob('../posts/*/*.json');
		$fileKeys = array_rand($files, $num);
		if (!is_array($fileKeys)) $fileKeys = array($fileKeys);
		$posts = array();
		foreach($fileKeys as $k) {
			$file = $files[$k];
			$post = new Post($file);
			array_push($posts, $post);
		}
		if ($num==1) return $posts[0];
		return $posts;
    }
    
    protected function notFound() {
        header("HTTP/1.0 404 Not Found");
        require('../templates/404.html');
        exit();
    }

	public function test() {
		
	}
}