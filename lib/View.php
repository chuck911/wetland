<?php

class View {
	public $templateDir = '../templates/';
	public $dataGenerator;

	public function __construct() {
		$this->dataGenerator = new DataGenerator();
	}

	public function evaluate($matches) {
		$name = $matches[1];
		$value = $this->dataGenerator->generate($name);
		if ($value === FALSE) return $matches[0];
		if (preg_match('|\{([^}]+)\}|',$value)) {
			return $this->renderTemplate($value);
		} else {
			return $value;
		}
	}
	
	public function preRender($filename, $data) {
        ob_start();
        extract($data);
		include("{$this->templateDir}{$filename}");
		return ob_get_clean();
	}
	
	public function render($filename, $data = array()) {
		$template = $this->preRender($filename, $data);
		return $this->renderTemplate($template);
	}
	
	public function renderTemplate($template) {
		$result = preg_replace_callback('|\{([^}]+)\}|',array($this,'evaluate'),$template);
		return $result;
	}
}