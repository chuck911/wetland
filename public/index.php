<?php
// require_once('../lib/EternalBread.php');

function autoload($class_name) {
	require '../lib/'.$class_name . '.php';
}

spl_autoload_register('autoload');

function cate($category){
	echo 'category: '.$category;
}
function sitemap() {
	echo 'xml,xml';
}

$app = new WebApp();
$app->route('/sitemap.xml', 'sitemap');
$app->route('/p/{tag}/{id}.html', array(new Controller, 'post'));
$app->route('/cate/{cate}', array(new Controller, 'index'));
$app->route('/tag/{tag}', array(new Controller, 'tag'));
$app->route('/', array(new Controller, 'index'));
$app->route('/test', array(new Controller, 'test'));
$app->run();