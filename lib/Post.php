<?php
class Post {
    public $file;
    public $id;
    public $title;
    public $content;
    public $tag;
    public $keyword;
    public $thumbnail;

    public function __construct($file) {
        $this->file = $file;
        $data = json_decode(file_get_contents($file),true);
        $this->id = basename($file,'.json');
        $paths = explode('/',$file);
        $this->tag = $paths[2];
        $this->title = $data['title'];
        $this->content = preg_replace('~本篇新闻相关链接：(.*)~','',$data['content']);
        $this->thumbnail = $data['thumbnail'];
        $this->keyword = $data['keyword'];
    }
    
    public function url() {
        return "/wetland/p/{$this->tag}/{$this->id}.html";
    }

    public function tag() {
        return new Tag($this->tag, $this->keyword);
    }
}
