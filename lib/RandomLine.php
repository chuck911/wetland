<?php
class RandomLine {
	public $singleFile = array(
		'外链'=>'host',
		'外部链接'=>'link',
		'随机目录'=>'list',
		'来源'=>'ly',
		'姓名'=>'name',
		'随机图片'=>'pics',
		'视频'=>'v',
	);
	public $multiFile = array(
		'新闻标题'=>'bt',
		'关键字'=>'key',
		'标题'=>'key',
		'内容'=>'txt'
	);
	public $dir = '../dbs/';
	public $customsDir = 'zdy/';

	protected $multiFile2 = null;

	public function get($name) {
		if (!$this->multiFile2) $this->multiFile2 = $this->transKeys($this->multiFile,'转码{key}');
		if (isset($this->singleFile[$name])) {
			$file = $this->dir.$this->singleFile[$name].'.txt';
			return $this->readRandomLine($file);
		} elseif (isset($this->multiFile[$name])) {
			$files = glob($this->dir.$this->multiFile[$name].'/*.txt');
			$file = $files[array_rand($files)];
			return $this->readRandomLine($file);
		} elseif (isset($this->multiFile2[$name])) {
			$files = glob($this->dir.$this->multiFile2[$name].'/*.txt');
			$file = $files[array_rand($files)];
			$line = $this->readRandomLine($file);
			return mb_convert_encoding($line, 'HTML-ENTITIES','GB2312');
		} else {
			$name = mb_convert_encoding($name,'UTF-8','GB2312');
			return $this->readRandomLine($this->dir.$this->customsDir.$name.'.txt');
		}
	}

	protected function transKeys($arr,$template) {
		$result = array();
		foreach($arr as $key => $value) {
			$newKey = str_replace('{key}',$key, $template);
			$result[$newKey] = $value;
		}
		return $result;
	}

	public function readRandomLine($filename) {
		$lines = @file($filename, FILE_SKIP_EMPTY_LINES);
		if ($lines===FALSE) return FALSE;
		if (!count($lines)) return null;
		return $lines[array_rand($lines)];
	}
}