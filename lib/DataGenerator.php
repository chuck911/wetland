<?php
class DataGenerator {
	public $methodsMap = array(
		'存放目录' => 'baseUrl',
		'当前目录' => 'currentDir',
		'随机目录' => 'randomDir',
		'数字' => 'randomNum',
		'字母' => 'randomChars',
		'字符' => 'randomChars',
		'年月日' => 'YMD',
		'年月' => 'YM',
		'月日' => 'MD',
		'时间' => 'time',
	);

	public function generate($name) {
		if (isset($this->methodsMap[$name])) {
			$method = $this->methodsMap[$name];
			return $this->$method();
		} elseif (method_exists($this, $name)) {
			return $this->$name();
		} else {
			$name = preg_replace('/(\d?)$/','',$name);
			return $this->randomLine($name);
		}
	}

	public function baseUrl() {
		return 'http://'.$_SERVER['SERVER_NAME'];
	}

	public function currentDir() {
		$pathInfo = pathinfo($_SERVER['REQUEST_URI']);
		return $pathInfo['dirname'];
	}

	public function randomDir() {
		return '/'.$this->randomString(8,'abcdefghijklmnopqrstuvwxyz');
	}

	public function randomChars() {
		return $this->randomString(3,'abcdefghijklmnopqrstuvwxyz');
	}

	public function YMD() { return @date('Y-m-d'); }
	public function MD() { return @date('md'); }
	public function YM() { return @date('Ym'); }
	public function time() { return @date('Y-m-d H:i:s'); }

	public function randomString($length = 10, $chars='0123456789abcdefghijklmnopqrstuvwxyz') {
		$charsLength = strlen($chars);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $chars[rand(0, $charsLength - 1)];
		}
		return $randomString;
	}

	public function randomLine($name) {
		$randomLine = new RandomLine();
		return $randomLine->get($name);
	}

	public function randomNum() {
		return rand(10,9999);
	}
}
