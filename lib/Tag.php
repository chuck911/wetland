<?php

class Tag {
    public $id;
    public $name;

    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
    }

    public function url() {
        return "/wetland/tag/{$this->id}";
    }

    public static function all() {
        $tagsData = require('../dbs/tags.php');
        $tags = [];
        foreach($tagsData as $id => $name) {
            array_push($tags, new Tag($id, $name));
        }
        return $tags;
    }

    public static function fromName($name) {
        $id2name = require('../dbs/tags.php');
        $name2id = array_flip($id2name);
        return new Tag($name2id[$name], $name);
    }

    public static function fromId($id) {
        $id2name = require('../dbs/tags.php');
        return new Tag($id, $id2name[$id]);
    }
}
